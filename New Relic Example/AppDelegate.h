//
//  AppDelegate.h
//  New Relic Example
//
//  Created by Christopher Mullins on 11/23/15.
//  Copyright © 2015 CM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

